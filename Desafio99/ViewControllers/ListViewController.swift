//
//  ListViewController.swift
//  Desafio99
//
//  Created by Lais on 10.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    @IBOutlet fileprivate var tableView: UITableView!
    
    var fileManager: FileManagerProtocol?
    fileprivate var people: [Person] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadPeople()
    }
}

// MARK: - Private Methods
extension ListViewController {
    fileprivate func loadPeople() {
        guard let fileManager = fileManager else { return }
        fileManager.loadDataSource(PersonDataSource.people, completion: { people, error in
            guard let people = people else {
                switch error! {
                case .notFound: self.showErrorMessage("File not found")
                case .invalidFormat: self.showErrorMessage("File has an invalid format")
                }
                return
            }
            self.people = people
        })
    }
    
    fileprivate func showErrorMessage(_ message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension ListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonCell", for: indexPath) as! PersonTableViewCell
        let viewModel = PersonViewModel(person: people[indexPath.row])
        cell.setup(viewModel: viewModel)
        return cell
    }
}
