//
//  FileManager.swift
//  Desafio99
//
//  Created by Lais on 10.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import Foundation

struct Resource<T> {
    let fileName: String
    let parse: (Any) throws -> T
}

enum FileError {
    case notFound
    case invalidFormat
}

protocol FileManagerProtocol {
    func loadDataSource<T>(_ resource: Resource<T>, completion: @escaping ((data: T?, error: FileError?)) -> ())
}

extension FileManager: FileManagerProtocol {}

final class FileManager {
    func loadDataSource<T>(_ resource: Resource<T>, completion: @escaping ((data: T?, error: FileError?)) -> ()) {
        guard let filePath = Bundle.main.path(forResource: resource.fileName, ofType: "json") else {
            completion((data: nil, error: .notFound))
            return
        }
        guard let fileData = try? Data(contentsOf: URL(fileURLWithPath: filePath)),
              let json = try? JSONSerialization.jsonObject(with: fileData),
              let resourceData = try? resource.parse(json) else {
            completion((data: nil, error: .invalidFormat))
            return
        }
        completion((data: resourceData, error: nil))
    }
}
