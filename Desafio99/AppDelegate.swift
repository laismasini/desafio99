//
//  AppDelegate.swift
//  Desafio99
//
//  Created by Lais on 10.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        if let rootViewController = window?.rootViewController as? ListViewController {
            rootViewController.fileManager = FileManager()
        }
        return true
    }
}

