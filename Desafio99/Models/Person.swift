//
//  Person.swift
//  Desafio99
//
//  Created by Lais on 10.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import Foundation
import Gloss

struct Person {
    let id: String
    let name: String
    let image: URL
    let birthday: Date
    let bio: String?
}

// MARK: - Decodable
extension Person: Decodable {
    init?(json: JSON) {
        guard let id: String = "id" <~~ json else { return nil }
        guard let name: String = "name" <~~ json else { return nil }
        guard let image: URL = "image" <~~ json else { return nil }
        guard let birthday: Date = Decoder.decode(dateISO8601ForKey: "birthday")(json) else { return nil }
        
        self.id = id
        self.name = name
        self.image = image
        self.birthday = birthday
        self.bio = "bio" <~~ json
    }
}

// MARK: - Hashable
extension Person: Hashable {
    var hashValue: Int {
        return id.hashValue
    }
}

// MARK: - Equatable
extension Person: Equatable {}
func ==(lhs: Person, rhs: Person) -> Bool {
    return lhs.id == rhs.id
}
