//
//  PersonDataSource.swift
//  Desafio99
//
//  Created by Lais on 11.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import Foundation

enum DataSourceError: Error {
    case invalidJSON
}

final class PersonDataSource {
    static let people = Resource<[Person]>(fileName: "data") { json in
        guard let responseArray = json as? [Dictionary<String, Any>] else {
            throw DataSourceError.invalidJSON
        }
        
        let personDataSource: Set<Person> = try Set(responseArray.map {
            guard let person = Person(json: $0) else {
                throw DataSourceError.invalidJSON
            }
            return person
        })
        return Array(personDataSource)
    }
}
