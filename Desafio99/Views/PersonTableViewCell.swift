//
//  PersonTableViewCell.swift
//  Desafio99
//
//  Created by Lais on 11.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import UIKit
import Kingfisher

final class PersonTableViewCell: UITableViewCell {
    @IBOutlet fileprivate var avatarImageView: UIImageView!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var birthdayLabel: UILabel!
    @IBOutlet fileprivate var bioLabel: UILabel!
    
    func setup(viewModel: PersonViewModel) {
        nameLabel.text = viewModel.name
        birthdayLabel.text = viewModel.birthday
        bioLabel.text = viewModel.bio
        avatarImageView.kf.setImage(with: viewModel.avatarURL)
    }
}
