//
//  PersonViewModel.swift
//  Desafio99
//
//  Created by Lais on 11.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import Foundation

final class PersonViewModel {
    fileprivate(set) var name: String
    fileprivate(set) var birthday: String
    fileprivate(set) var bio: String?
    fileprivate(set) var avatarURL: URL
    
    init(person: Person) {
        self.name = person.name
        self.birthday = "Born in \(Formatter.dateFormatter.string(from: person.birthday))"
        self.bio = person.bio
        self.avatarURL = person.image
    }
}

// MARK: - Formatter
extension Formatter {
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        return formatter
    }()
}
