//
//  PersonViewModelTests.swift
//  Desafio99
//
//  Created by Lais on 12.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import XCTest
@testable import Desafio99

final class PersonViewModelTests: XCTestCase {
    func testShouldFormatPersonData() {
        let personJSON = [
            "id": "599df0e4-183c-4bfb-91c5-881d4550cd3f",
            "name": "Craig Federighi",
            "image": "http://www.apple.com/pr/bios/images/federighi_hero20120727.png",
            "birthday": "1969-05-27T00:00:00Z",
            "bio": "Craig Federighi is Apple's senior vice president of Software Engineering. Federighi oversees the development of iOS, macOS and Apple's common operating system engineering teams."
        ]
        guard let person = Person(json: personJSON) else {
            XCTFail("Unable to convert person object")
            return
        }
        let viewModel = PersonViewModel(person: person)
        
        XCTAssertEqual(viewModel.name, person.name, "Name should be equal")
        XCTAssertEqual(viewModel.avatarURL, person.image, "Image URL should be equal")
        let birthdayString = "Born in \(Formatter.dateFormatter.string(from: person.birthday))"
        XCTAssertEqual(viewModel.birthday, birthdayString, "Birthday string should be equal")
        XCTAssertEqual(viewModel.bio, person.bio, "Bio should be equal")
    }
}
