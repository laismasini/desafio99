//
//  PersonTests.swift
//  Desafio99
//
//  Created by Lais on 10.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import XCTest
@testable import Desafio99

final class PersonTests: XCTestCase {
    let personJSON = [
        "id": "599df0e4-183c-4bfb-91c5-881d4550cd3f",
        "name": "Craig Federighi",
        "image": "http://www.apple.com/pr/bios/images/federighi_hero20120727.png",
        "birthday": "1969-05-27T00:00:00Z",
        "bio": "Craig Federighi is Apple's senior vice president of Software Engineering. Federighi oversees the development of iOS, macOS and Apple's common operating system engineering teams."
    ]
    
    func testShouldParseAPersonObject() {
        guard let person = Person(json: personJSON) else {
            XCTFail("Unable to convert person object")
            return
        }
        XCTAssertEqual(personJSON["id"], person.id, "ID should be equal")
        XCTAssertEqual(personJSON["name"], person.name, "Name should be equal")
        XCTAssertEqual(URL(string: personJSON["image"]!), person.image, "Image URL should be equal")
        XCTAssertEqual(Formatter.iso8601.date(from: personJSON["birthday"]!), person.birthday, "Birthday date should be equal")
        XCTAssertEqual(personJSON["bio"], person.bio!, "Bio should be equal")
    }
    
    func testPersonShouldBeComparable() {
        let equalJSON = [
            "id": "599df0e4-183c-4bfb-91c5-881d4550cd3f",
            "name": "Craig Federighi",
            "image": "http://www.apple.com/pr/bios/images/federighi_hero20120727.png",
            "birthday": "1969-05-27T00:00:00Z",
            "bio": "Craig Federighi is Apple's senior vice president of Software Engineering. Federighi oversees the development of iOS, macOS and Apple's common operating system engineering teams."
        ]
        
        let differentJSON = [
            "id": "145854f5-63a7-456a-9532-9f3a2fcf9875",
            "name": "Jonathan Ive",
            "image": "http://d.fastcompany.net/multisite_files/codesign/imagecache/1280/poster/2012/10/1671131-poster-1280-jony-ive.jpg",
            "birthday": "1967-02-27T00:00:00Z",
            "bio": "Sir Jonathan Paul Ive, KBE, is a British industrial designer who is currently the Chief Design Officer of Apple Inc."
        ]
        
        guard let equalPerson = Person(json: equalJSON),
              let differentPerson = Person(json: differentJSON),
              let originalPerson = Person(json: personJSON) else {
                XCTFail("Unable to convert person objects")
                return
        }
        
        XCTAssertEqual(originalPerson, equalPerson, "Different Person objects with same ID should be equal")
        XCTAssertNotEqual(originalPerson, differentPerson, "Different Person objects with different ID should not be equal")
        XCTAssertEqual(originalPerson.hashValue, equalPerson.hashValue, "Hash values should be equal")
        XCTAssertNotEqual(originalPerson.hashValue, differentPerson.hashValue, "Hash values should not be equal")
        XCTAssertEqual(originalPerson.id.hashValue, equalPerson.hashValue, "Person hash value should match ID hash value")
    }
}

// MARK: - Formatter
extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return formatter
    }()
}
