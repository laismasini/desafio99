//
//  ListViewControllerTests.swift
//  Desafio99
//
//  Created by Lais on 12.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import XCTest
@testable import Desafio99

class ListViewControllerTests: XCTestCase {
    func testShouldCallLoadPeople() {
        class ListViewControllerMock: ListViewController {
            var viewControllerToPresent: UIViewController?
            
            override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Swift.Void)? = nil) {
                self.viewControllerToPresent = viewControllerToPresent
            }
        }
        
        class FileManagerMock: FileManagerProtocol {
            var isLoaded: Bool = false
            func loadDataSource<T>(_ resource: Resource<T>, completion: @escaping ((data: T?, error: FileError?)) -> ()) {
                isLoaded = true
            }
        }
        
        let listViewController: ListViewControllerMock = ListViewControllerMock()
        let fileManagerMock = FileManagerMock()
        listViewController.fileManager = fileManagerMock
        UIApplication.shared.keyWindow?.rootViewController = listViewController
        
        XCTAssertTrue(fileManagerMock.isLoaded, "Method loadPeople should be called on viewDidLoad")
    }
    
    func testShouldPresentFileNotFoundErrorAlert() {
        class ListViewControllerMock: ListViewController {
            var viewControllerToPresent: UIViewController?
            
            override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Swift.Void)? = nil) {
                self.viewControllerToPresent = viewControllerToPresent
            }
        }
        
        class FileManagerMock: FileManagerProtocol {
            func loadDataSource<T>(_ resource: Resource<T>, completion: @escaping ((data: T?, error: FileError?)) -> ()) {
                completion((data: nil, error: .notFound))
            }
        }
        
        let listViewController: ListViewControllerMock = ListViewControllerMock()
        let fileManagerMock = FileManagerMock()
        listViewController.fileManager = fileManagerMock
        UIApplication.shared.keyWindow?.rootViewController = listViewController
        
        guard let alert = listViewController.viewControllerToPresent as? UIAlertController else {
            XCTFail("UIAlertController failed to be presented")
            return
        }
        XCTAssertEqual("Error", alert.title, "Alert title should be equal")
        XCTAssertEqual("File not found", alert.message, "Alert message should be File not found")
    }
    
    func testShouldPresentInvalidFormatErrorAlert() {
        class ListViewControllerMock: ListViewController {
            var viewControllerToPresent: UIViewController?
            
            override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Swift.Void)? = nil) {
                self.viewControllerToPresent = viewControllerToPresent
            }
        }
        
        class FileManagerMock: FileManagerProtocol {
            func loadDataSource<T>(_ resource: Resource<T>, completion: @escaping ((data: T?, error: FileError?)) -> ()) {
                completion((data: nil, error: .invalidFormat))
            }
        }
        
        let listViewController: ListViewControllerMock = ListViewControllerMock()
        let fileManagerMock = FileManagerMock()
        listViewController.fileManager = fileManagerMock
        UIApplication.shared.keyWindow?.rootViewController = listViewController
        
        guard let alert = listViewController.viewControllerToPresent as? UIAlertController else {
            XCTFail("UIAlertController failed to be presented")
            return
        }
        XCTAssertEqual("Error", alert.title, "Alert title should be equal")
        XCTAssertEqual("File has an invalid format", alert.message, "Alert message should be File has an invalid format")
    }
}
