//
//  FileManagerTests.swift
//  Desafio99
//
//  Created by Lais on 11.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import XCTest
@testable import Desafio99

class FileManagerTests: XCTestCase {
    func testShouldLoadDataSource() {
        let resource = Resource<Any>(fileName: "data") { json in
            return json
        }
        FileManager().loadDataSource(resource, completion: { data, error in
            XCTAssertNotNil(data, "Should be able to load data")
            XCTAssertNil(error, "Should not return an error")
        })
    }
    
    func testShouldReturnNotFoundFileError() {
        let resource = Resource<Any>(fileName: "person") { json in
            return json
        }
        
        FileManager().loadDataSource(resource, completion: { data, error in
            XCTAssertNil(data, "Should not load data")
            XCTAssertEqual(error, .notFound, "Should return not found error")
        })
    }
    
    func testShouldReturnInvalidFormatError() {
        let resource = Resource<Any>(fileName: "data") { json in
            throw DataSourceError.invalidJSON
        }
        
        FileManager().loadDataSource(resource, completion: { data, error in
            XCTAssertNil(data, "Should not load data")
            XCTAssertEqual(error, .invalidFormat, "Should return not found error")
        })
    }
}
