//
//  PersonDataSourceTests.swift
//  Desafio99
//
//  Created by Lais on 11.06.17.
//  Copyright © 2017 Lais Masini. All rights reserved.
//

import XCTest
@testable import Desafio99

class PersonDataSourceTests: XCTestCase {
    func testShouldReturnAnArray() {
        FileManager().loadDataSource(PersonDataSource.people, completion: { people, error in
            XCTAssertEqual(people?.count, 6, "Should return 5 items")
        })
    }
    
    func testShouldContainAPerson() {
        FileManager().loadDataSource(PersonDataSource.people, completion: { people, error in
            XCTAssertNotNil(people?.filter({ $0.name == "Steve Jobs" }), "Should contain a person named Steve Jobs")
        })
    }
}
